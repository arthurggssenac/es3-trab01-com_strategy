/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastropessoasstrategy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author arthu_qf7f61o
 */
public class Cadastro implements FuncaoPessoas {
    
    @Override
    public ArrayList<Pessoa> funcao(ArrayList<Pessoa> pessoas){
        try {
            BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
            
            Pessoa novaPessoa = new Pessoa();
            System.out.println("Insira o nome da pessoa: ");
            novaPessoa.setNome(teclado.readLine());
            System.out.println("Insira a idade da pessoa: ");
            novaPessoa.setIdade(Integer.parseInt(teclado.readLine()));
            System.out.println("Insira o sexo da pessoa: ");
            novaPessoa.setSexo(teclado.readLine());
            System.out.println("Insira o CPF da pessoa: ");
            novaPessoa.setCpf(teclado.readLine());
            System.out.println("Insira o saldo da pessoa: ");
            novaPessoa.setSaldo(Double.parseDouble(teclado.readLine()));
            
            pessoas.add(novaPessoa);
            
            return pessoas;
        } catch (IOException ex) {
            Logger.getLogger(Cadastro.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pessoas;
    }
}
