/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastropessoasstrategy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author arthu_qf7f61o
 */
public class Excluir implements FuncaoPessoas {
    
    @Override
    public ArrayList<Pessoa> funcao(ArrayList<Pessoa> pessoas){
        try {
            BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
            int escolha;
            
            System.out.println("Indique a posição da pessoa a ser excluida: ");
            escolha = Integer.parseInt(teclado.readLine());
            
            pessoas.remove(escolha);
            
            return pessoas;
        } catch (IOException ex) {
            Logger.getLogger(Excluir.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pessoas;
    }
}

