/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastropessoasstrategy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author arthu_qf7f61o
 */
public class Editar implements FuncaoPessoas {
    
    @Override
    public ArrayList<Pessoa> funcao(ArrayList<Pessoa> pessoas){
        try {
            BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
            int escolha;
            
            System.out.println("Indique a posição da pessoa a ser editada: ");
            escolha = Integer.parseInt(teclado.readLine());
            Pessoa velhaPessoa = pessoas.get(escolha);
            
            
            System.out.println("nome: " + velhaPessoa.getNome());
            System.out.println("Insira o nome da pessoa: ");
            velhaPessoa.setNome(teclado.readLine());
            
            System.out.println("Idade: " + velhaPessoa.getIdade());
            System.out.println("Insira a idade da pessoa: ");
            velhaPessoa.setIdade(Integer.parseInt(teclado.readLine()));
            
            System.out.println("Sexo: " + velhaPessoa.getSexo());
            System.out.println("Insira o sexo da pessoa: ");
            velhaPessoa.setSexo(teclado.readLine());
            
            System.out.println("CPF: " + velhaPessoa.getCpf());
            System.out.println("Insira o CPF da pessoa: ");
            velhaPessoa.setCpf(teclado.readLine());
            
            System.out.println("Saldo: " + velhaPessoa.getSaldo());
            System.out.println("Insira o saldo da pessoa: ");
            velhaPessoa.setSaldo(Double.parseDouble(teclado.readLine()));
            
            pessoas.set(escolha, velhaPessoa);
            
            return pessoas;
        } catch (IOException ex) {
            Logger.getLogger(Editar.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pessoas;
    }
}
