/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastropessoasstrategy;

import java.util.ArrayList;

/**
 *
 * @author arthu_qf7f61o
 */
public class Listar implements FuncaoPessoas {
    
    @Override
    public ArrayList<Pessoa> funcao(ArrayList<Pessoa> pessoas){
        
        int i = 0;
        for (Pessoa pessoa: pessoas) {
            System.out.println("Posição: " + i + " nome: " + pessoa.getNome() + ""
                    + " idade: " + pessoa.getIdade() + " sexo: " + pessoa.getSexo() + ""
                    + " cpf: " + pessoa.getCpf() + " saldo: " + pessoa.getSaldo());
            i++;
        }
        
        return pessoas;
    }
}
