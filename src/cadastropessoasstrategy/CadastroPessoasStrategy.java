/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadastropessoasstrategy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 *
 * @author arthu_qf7f61o
 */
public class CadastroPessoasStrategy {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
        int escolha;
        
        ArrayList<Pessoa> pessoas = new ArrayList<Pessoa>();
        
        FuncaoPessoas funcaoCadastro = new Cadastro();
        FuncaoPessoas funcaoEditar = new Editar();
        FuncaoPessoas funcaoExcluir = new Excluir();
        FuncaoPessoas funcaoListar = new Listar();
        
        while(true){
        
            System.out.println("##Bosstrategy##");
            System.out.println("Cadastro de pessoas");
            System.out.println("Escolha a sua opção:");
            System.out.println("1 - Cadastrar nova pessoa");
            System.out.println("2 - Editar velha pessoa");
            System.out.println("3 - Excluir pessoa");
            System.out.println("4 - Listar pessoas");

            escolha = Integer.parseInt(teclado.readLine());
            
            switch (escolha) {
                case 1:
                    pessoas = funcaoCadastro.funcao(pessoas);

                    pessoas = funcaoListar.funcao(pessoas);
                    break;
                case 2:
                    pessoas = funcaoListar.funcao(pessoas);
                    
                    pessoas = funcaoEditar.funcao(pessoas);
                    
                    pessoas = funcaoListar.funcao(pessoas);
                    break;
                case 3:
                    pessoas = funcaoListar.funcao(pessoas);
                    
                    pessoas = funcaoExcluir.funcao(pessoas);
                    
                    pessoas = funcaoListar.funcao(pessoas);
                    break;
                case 4:
                    pessoas = funcaoListar.funcao(pessoas);
                    
                    break;
                default:
                    throw new AssertionError();
            }
        }
    }
    
}
